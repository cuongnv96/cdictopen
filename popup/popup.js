var VOICE_URL = "https://translate.google.com.vn/translate_tts?ie=UTF-8&q={text}&tl={from-lang}&total=1&idx=0&textlen=6&client=gtx&prev=input";

var DEFAULT_FROM = 1;
var DETAULT_TO 	 = 2;

const KEY_LANG_FROM = "lang_from";
const KEY_LANG_TO	= "lang_to";

const SOURCE = "word_source";
const LAST_TRANS_RESULT = "last_trans_result";

// actionId
var ACT_TRANSLATE 		 = "translate";
var ACT_TRANSLATE_RESULT = "translateResult";

var resultWaiting;

$(document).ready(function() {
	initApp();
});

function initApp() {
	chrome.runtime.onMessage.addListener(handleResponse);

	storage.get(KEY_LANG_FROM, (value) => {
		if (value != null) {
			$("#sl-from")[0].selectedIndex = value;
		}
	});

	storage.get(KEY_LANG_TO, (value) => {
		if (value != null) {
			$("#sl-to")[0].selectedIndex = value;
		}
	})

	$("#info-split").hide();
	$("#trans-voice").hide();

	var sourceView = $("#txt-source");
	sourceView.focus();	
	sourceView.keydown(function() {
		if (event.keyCode == 13) {
			var source = sourceView.val().trim();
			sourceView.val(source);

			doTranslate(source);
		}
	});

	var voiceView = $("#trans-voice");
	voiceView.click(function() {
		playSound();
	});

	$("#lang-switch").click(function() {
		var leftSelect = $("#sl-from")[0].selectedIndex;
		var rightSelect = $("#sl-to")[0].selectedIndex;
		$("#sl-from")[0].selectedIndex = rightSelect;
		$("#sl-to")[0].selectedIndex = leftSelect;

		storage.set(KEY_LANG_TO, $("#sl-to")[0].selectedIndex);
		storage.set(KEY_LANG_FROM, $("#sl-from")[0].selectedIndex);
	});

	$("#sl-from").on("change", function() {
		storage.set(KEY_LANG_FROM, $("#sl-from")[0].selectedIndex);
	});

	$("#sl-to").on("change", function() {
		storage.set(KEY_LANG_TO, $("#sl-to")[0].selectedIndex);
	});

	storage.get(SOURCE, (source) => {
		resultWaiting = source;
		sourceView.text(resultWaiting);
	});

	storage.get(LAST_TRANS_RESULT, (request) => {
		updateResult(request, false);
	});
};

function playSound() {
	var audio = new Audio();
	audio.src = VOICE_URL.replace("{text}", resultWaiting)
						 .replace("{from-lang}", $("#sl-from").val());
	audio.play();
}

function doTranslate(source) {
	$("#txt-source").prop("disabled", true);
	resultWaiting = source;

	storage.set(SOURCE, resultWaiting);

	chrome.runtime.sendMessage({
		actionId: ACT_TRANSLATE,
		fromLang: $("#sl-from").val(),
		toLang: $("#sl-to").val(),
		data: source
	});
};

function handleResponse(request, response, sendResponse) {
	if (request.actionId === ACT_TRANSLATE_RESULT) {
		if (request.resultId == resultWaiting) {
			updateResult(request, true);
		}
	}
};

function updateResult(request, sound) {
	storage.set(LAST_TRANS_RESULT, request);

	$("#txt-source").prop("disabled", false);
	$("#txt-source").focus();
	$("#txt-source").select();

	// main
	safeRun(() => {
		$("#trans-text").text(request.result[0][0][0]);
		$("#trans-voice").show();

		if (sound) {
			playSound();
		}
	}, () => {
		$("#trans-text").text("[no data]");
		$("#trans-voice").hide();
	});

	// speech
	safeRun(() => {
		var speech = request.result[0][1][3];
		if (speech.trim() == "") {
			$("#trans-speech").text("[no data]");
		} else {
			$("#trans-speech").text(speech);
		}
	}, () => {
		$("#trans-speech").text("[no data]");
	});

	// info
	safeRun(() => {
		var sumInfoText = "";
		var iNewLine = 0;
		for (var iv = 0; iv < 5; iv++) {
			if (typeof(request.result[1][iv]) != "undefined") {
				var infoHeader = request.result[1][iv][0];
				var infoDetail = request.result[1][iv][1];
				var headerCapital = infoHeader.charAt(0).toUpperCase() + infoHeader.slice(1);
				var infoText = "<b>" + headerCapital + ": </b>" + infoDetail.join(", ");

				if (iNewLine > 0) sumInfoText += "<br />";
				sumInfoText += infoText;
				iNewLine++;
			}
		}
		$("#trans-info").html(sumInfoText);
		$("#info-split").show();
	}, () => {
		$("#trans-info").html("");
		$("#info-split").hide();
	});
};

function safeRun(job, fail) {
	try {
		job();
	} catch (e) {
		console.log(e);
		if (fail != null) {
			fail();
		}
	}
};