var TRANS_URL = "https://translate.google.com.vn/translate_a/single?client=gtx&sl={from-lang}&tl={to-lang}&hl={to-lang}&dt=at&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&ie=UTF-8&oe=UTF-8&otf=1&ssel=0&tsel=0&kc=3&q={keyword}";

// actionId
var ACT_TRANSLATE = "translate";
var ACT_TRANSLATE_RESULT = "translateResult";

chrome.runtime.onMessage.addListener(handleRequest);

function handleRequest(request, response, sendResponse) {
    if (request.actionId === ACT_TRANSLATE) {
        translateText(request, response);        
    }
};

function translateText(request, response) {
    $.ajax({
        url: TRANS_URL.replace("{from-lang}", request.fromLang)
                      .replace("{to-lang}", request.toLang)
                      .replace("{keyword}", request.data),
        dataType: 'json',
        success: function(data) {
            chrome.runtime.sendMessage({
                actionId: ACT_TRANSLATE_RESULT,
                resultId: request.data,
                result: data
            });
        },
        error: function(err) {
            console.log(err);
            chrome.runtime.sendMessage({
                actionId: ACT_TRANSLATE_RESULT,
                resultId: request.data,
                result: null
            });
        }
      });
};

$(document).ready(() => {
    chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
	    if (changeInfo.status != 'complete')
	        return;

        $(document).ready(function() {
            document.onkeyup = function(event) {
                if (event.ctrlKey && event.keyCode == 0) {
                    alert(event);
                    console.log(event);
                }
            }
        });
	});
});